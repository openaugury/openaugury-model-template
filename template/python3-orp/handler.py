from orp_sim_firstgen.result_service import ResultService
from orp_sim_firstgen.feature import Feature
from orp_sim_firstgen.plot import get_cached_50m

from flask import current_app

import asyncio
import os
from minio import Minio
from minio.error import ResponseError as MinioResponseError

import io
import logging
import json
import tempfile
import requests
from flask_restful import Resource, abort, reqparse
import function.phenomenon as mod


class Handler(Resource):
    _bucket = None
    _config = {}
    step = 3000

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('id', location='json')
        parser.add_argument('version', location='json')
        parser.add_argument('phenomenon', location='json')
        parser.add_argument('definition', location='json')
        parser.add_argument('model', location='json')
        parser.add_argument('begin', location='json')
        parser.add_argument('end', location='json')
        parser.add_argument('center', location='json')
        parser.add_argument('window', location='json')
        parser.add_argument('minio_key', location='json')
        args = parser.parse_args()

        window = json.loads(args['window'])
        center = json.loads(args['center'])

        feature_sets = {}
        current_app.logger.info(args)

        current_app.logger.error('Opening minio: ' + args['minio_key'])
        key = args['minio_key']

        try:
            data_object = self._mo_client.get_object(self._mo_bucket, 'simulations/{}.json'.format(key))
            stream_req = io.BytesIO()
            for d in data_object.stream(32 * 1024):
                stream_req.write(d)
        except MinioResponseError as err:
            current_app.logger.error(err)

        features = stream_req.getvalue().decode('utf-8')

        for feature_set in json.loads(features):
            feature_sets[feature_set['set_id']] = []
            for fs in feature_set['chunk']:
                coordinates = fs['location']['coordinates']
                coordinates.reverse()
                feature_sets[feature_set['set_id']].append(Feature(fs['feature_id'], coordinates))

        if args['version'] != '2':
            times = [int(args['time'])]
        else:
            times = range(0, int(args['end']) - int(args['begin']), self.step)

        if args['definition']:
            definition = json.loads(args['definition'])
            risk_parameters = definition['parameters']
            if not risk_parameters:
                risk_parameters = {}
        else:
            risk_parameters = {}

        risk = mod.risk()
        preload = risk.preload()

        feature_sets, calc_results = self.result_service.retrieve(risk, times, risk_parameters, feature_sets, center[0], center[1], *window)

        results = []
        for result in calc_results:
            time, points, f, grid = result

            points = {"%d,%d" % k: v for k, v in points.items()}

            if args['begin']:
                time += int(args['begin'])
            results.append({'time': time, 'points': points, 'grid': grid})

        if args['version'] != '2':
            results = results[0]

        feature_arcs = {str(k): [l.healthDict() for l in v] for k, v in feature_sets.items()}

        current_app.logger.error('Finished')
        current_app.logger.error({'results': len(results), 'feature_arcs': len(feature_arcs)})

        if key and args['minio_key'] and self._mo_client:
            try:
                data_object = self._mo_client.remove_object(self._mo_bucket, 'simulations/{}.json'.format(key))
            except MinioResponseError as err:
                current_app.logger.error(err)

            serial_res = bytes(json.dumps(results), encoding='utf-8')
            stream_res = io.BytesIO(serial_res)

            key_result = '{}_result'.format(key)
            try:
                self._mo_client.put_object(
                    self._mo_bucket,
                    'simulations/{}.json'.format(key_result),
                    stream_res,
                    len(serial_res)
                )
            except MinioResponseError as err:
                current_app.logger.error(err)

            serial_res = bytes(json.dumps(feature_arcs), encoding='utf-8')
            stream_res = io.BytesIO(serial_res)

            key_feature_arcs = '{}_feature_arcs'.format(key)
            try:
                self._mo_client.put_object(
                    self._mo_bucket,
                    'simulations/{}.json'.format(key_feature_arcs),
                    stream_res,
                    len(serial_res)
                )
            except MinioResponseError as err:
                current_app.logger.error(err)

            return {'results': key_result, 'feature_arcs': key_feature_arcs}
        else:
            return {'results': results, 'feature_arcs': feature_arcs}
        return result

    @classmethod
    def preload(cls):
        mo_config = {'region': 'us-east-1'}
        for k in ('bucket', 'key', 'secret', 'endpoint'):
            filename = os.path.join('/var', 'openfaas', 'secrets', f'minio_{k}')
            with open(filename, 'r') as f:
                value = f.read().strip()
            mo_config[k] = value

        #if hasattr(mod, 'preload'):
        #    mod.preload()
        cls._mo_client = Minio(
            mo_config['endpoint'],
            access_key=mo_config['key'],
            secret_key=mo_config['secret'],
            region=mo_config['region'],
            secure=True
        )
        cls._mo_bucket = mo_config['bucket']

        #debug = cls._config['debug'] if 'debug' in cls._config else False
        logging.basicConfig() #level=logging.DEBUG if debug else logging.INFO)
        cls.logger = logging.getLogger(__name__)

        cls.result_service = ResultService()  # creating new ResultService object
        cls.result_service.preload(mod.risk())

        return True
